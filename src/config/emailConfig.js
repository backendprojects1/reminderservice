const nodemailer = require('nodemailer');
const {EMAIL_ID, EMAIL_PASS} = require('./serverConfig');

/* parameter 1: name of service
   parameter 2: auth --> which is an object in itself
*/

const sender = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: EMAIL_ID,
        pass: EMAIL_PASS
    }
});

module.exports = sender;